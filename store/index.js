export const state = () => ({
  rawData: "",
  searchData: ""
})

export const mutations = {
  assignRawdata(state, data) {
    state.rawData = data
    state.searchData = data
  },
  searching(state, data) {
    state.searchData = data
  }
}

export const actions = {
  assignRawdata(context, data) {
    context.commit("assignRawdata", data)
  },
  searching(context, data) {
    context.commit("searching", data)
  }
}
