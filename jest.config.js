module.exports = {
  verbose: true,
  collectCoverage: true,
  coverageDirectory: 'test/coverage',
  reporters: [
    'default',
    ['./node_modules/jest-html-reporter', {
      pageTitle: 'Front End Unit Test Report',
      outputPath: 'test/reports/index.html',
    }],
  ],
  coveragePathIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/dist/',
    '<rootDir>/layouts/',
    '<rootDir>/locales/',
    '<rootDir>/middleware/',
    '<rootDir>/plugins/',
    '<rootDir>/sdet_test/',
    '<rootDir>/server/',
    '<rootDir>/static/',
    '<rootDir>/test/',
  ],
  moduleNameMapper: {
    '\\.(css|less|sass|scss)$': '<rootDir>/__mocks__/styleMock.js',
    '\\.(gif|ttf|eot|svg)$': '<rootDir>/__mocks__/fileMock.js',
    '(vuejs-datepicker/dist/locale)$': '<rootDir>/__mocks__/fileMock.js',
    '^@/(.*)$': '<rootDir>/$1',
    '^~/(.*)$': '<rootDir>/$1',
    '^vue$': 'vue/dist/vue.common.js',
  },
  moduleFileExtensions: ['js', 'vue', 'json'],
  transform: {
  '^.+\\.js$': '<rootDir>/node_modules/babel-jest',
  '.*\\.(vue)$': '<rootDir>/node_modules/vue-jest',
  },
  snapshotSerializers: ['<rootDir>/node_modules/jest-serializer-vue'],
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
};