import { Get, Post } from "~/utils/ApiCall.js"

export default function ApiMix(type, url, payload) {
  let callObj

  switch (type) {
    case "GET":
      callObj = Get(url)
      break
    case "POST":
      callObj = Post(url, payload)
      break
  }

  const mixinObj = {
    data() {
      return {
        responseData: ""
      }
    },
    methods: {
      callApionMount() {
        console.log('masuk apimix')
        callObj.then(res => {
          this.responseData = res
        })
      }
    },
    created() {
      this.callApionMount()
    }
  }

  return mixinObj
}
