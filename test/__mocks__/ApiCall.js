const mockData = {
  "/locations" : [
    {
      "id": "11014596-71b0-4b3e-b8c0-1c4b15f28b9a",
      "name": "Irontown",
      "climate": "Continental",
      "terrain": "Mountain",
      "surface_water": "40",
      "residents": [],
      "films": [],
      "url": []
    },
    {
      "id": "11014596-71b0-4b3e-b8c0-1c4b15f28b9a",
      "name": "Gutiokipanja",
      "climate": "Continental",
      "terrain": "Hill",
      "surface_water": "50",
      "residents": [],
      "films": [],
      "url": []
    }
  ]
}

export function Get(url) {
  // console.log('masuk api mock')
  var data = null;
  for(let prop in mockData) {
    if (url.search(mockData[prop])) {
      data = mockData[prop]
    }
  }
  return new Promise((resolve, reject) => {
    if (data != null) {
      resolve(data)
    } else {
      reject('no matching with available mock data')
    }
  })
}