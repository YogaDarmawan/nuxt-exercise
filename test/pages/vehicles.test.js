import { mount, createLocalVue } from "@vue/test-utils"
import Vuex from "vuex";
import Vehicles from "~/pages/vehicles/index.vue"
import VehicleData from "~/utils/data/VehicleData.js"
import DetailBanner from "~/components/render/DetailBanner.vue"

const localVue = createLocalVue()

localVue.use(Vuex)

const store = new Vuex.Store({
  state: {
    searchData: ""
  },
  mutations: {
    assignRawdata(state, data) {
      state.rawData = data
      state.searchData = data
    },
    searching(state, data) {
      state.searchData = data
    }
  },
  actions: {
    assignRawdata(context, data) {
      context.commit("assignRawdata", data)
    },
    searching(context, data) {
      context.commit("searching", data)
    }
  }
})

describe('VEHICLES.vue', () => {
  const wrapper = mount(Vehicles, {
    localVue,
    store,
    stubs: {
      DetailBanner: true
    }
  })
  test('Render pages Vehicles', () => {
    expect(true).toBe(true)
  })
  test('[PAGES - VEHICLES] Load data to Store then to local State', () => {
    expect(wrapper.vm.searchData).toBe(VehicleData)
    expect(store.state.searchData).toBe(VehicleData)
  })
  test('[PAGES - VEHICLES] Click Card Media Custom expect Banner to pop up', () => {
    const card = wrapper.find('.card-media-custom')
    card.trigger('click')
    expect(wrapper.find(DetailBanner).exists()).toBe(true)
  })
})