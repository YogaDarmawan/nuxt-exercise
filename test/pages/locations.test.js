import { mount, createLocalVue } from "@vue/test-utils"
import Vuex from "vuex";
import Locations from "~/pages/locations/index.vue"
import DetailBanner from "~/components/render/DetailBanner.vue";
import { Get } from "../__mocks__/ApiCall";

const localVue = createLocalVue()

localVue.use(Vuex)

const store = new Vuex.Store({
  state: {
    searchData: ""
  },
  mutations: {
    assignRawdata(state, data) {
      state.rawData = data
      state.searchData = data
    },
    searching(state, data) {
      state.searchData = data
    }
  },
  actions: {
    assignRawdata(context, data) {
      context.commit("assignRawdata", data)
    },
    searching(context, data) {
      context.commit("searching", data)
    }
  }
})

describe('Locations.vue', () => {
  const wrapper = mount(Locations, {
    localVue,
    store,
    stubs: {
      DetailBanner: true
    },
    methods: {
      callApionMount() {
        Get('/locations')
        .then(res => {
          this.responseData = res
        })
      }
    }
  })
  test('Render pages Locations', () => {
    expect(true).toBe(true)
  })
  test('[PAGES - Locations] Load data to Store then to local State', () => {
    // console.log(wrapper.vm.searchData)
    let localSearchData = wrapper.vm.searchData

    expect(localSearchData).not.toBe("")
    expect(store.state.searchData).toBe(localSearchData)
  })
  test('[PAGES - Locations] Click Card Media Custom expect Banner to pop up', () => {
    const card = wrapper.find('.card-media-custom')
    // console.log(card)
    card.trigger('click')
    expect(wrapper.find(DetailBanner).exists()).toBe(true)
  })
})