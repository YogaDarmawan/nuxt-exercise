import { mount, createLocalVue } from "@vue/test-utils"
import Vuex from "vuex";
import People from "~/pages/people/index.vue"
import PeopleData from "~/utils/data/PeopleData.js"
import DetailBanner from "~/components/render/DetailBanner.vue"

const localVue = createLocalVue()

localVue.use(Vuex)

const store = new Vuex.Store({
  state: {
    searchData: ""
  },
  mutations: {
    assignRawdata(state, data) {
      state.rawData = data
      state.searchData = data
    },
    searching(state, data) {
      state.searchData = data
    }
  },
  actions: {
    assignRawdata(context, data) {
      context.commit("assignRawdata", data)
    },
    searching(context, data) {
      context.commit("searching", data)
    }
  }
})

describe('People.vue', () => {
  const wrapper = mount(People, {
    localVue,
    store,
    stubs: {
      DetailBanner: true
    }
  })
  test('Render pages People', () => {
    expect(true).toBe(true)
  })
  test('[PAGES - PEOPLE] Load data to Store then to local State', () => {
    expect(wrapper.vm.searchData).toBe(PeopleData)
    expect(store.state.searchData).toBe(PeopleData)
  })
  test('[PAGES - PEOPLE] Click Card Media Custom expect Banner to pop up', () => {
    const card = wrapper.find('.card-media-custom')
    card.trigger('click')
    expect(wrapper.find(DetailBanner).exists()).toBe(true)
  })
})