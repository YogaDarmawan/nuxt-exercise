import { mount, createLocalVue } from "@vue/test-utils"
import Vuex from "vuex";
import Films from "~/pages/films/index.vue"
import FilmsData from "~/utils/data/FilmsData.js"
import DetailBanner from "~/components/render/DetailBanner.vue"

const localVue = createLocalVue()

localVue.use(Vuex)

const store = new Vuex.Store({
  state: {
    searchData: ""
  },
  mutations: {
    assignRawdata(state, data) {
      state.rawData = data
      state.searchData = data
    },
    searching(state, data) {
      state.searchData = data
    }
  },
  actions: {
    assignRawdata(context, data) {
      context.commit("assignRawdata", data)
    },
    searching(context, data) {
      context.commit("searching", data)
    }
  }
})

describe('Films.vue', () => {
  const wrapper = mount(Films, {
    localVue,
    store,
    stubs: {
      DetailBanner: true
    }
  })
  test('Render pages Films', () => {
    expect(true).toBe(true)
  })
  test('[PAGES - FILMS] Load data to Store then to local State', () => {
    expect(wrapper.vm.searchData).toBe(FilmsData)
    expect(store.state.searchData).toBe(FilmsData)
  })
  test('[PAGES - FILMS] Click Card Media Custom expect Banner to pop up', () => {
    const card = wrapper.find('.card-media-custom')
    card.trigger('click')
    expect(wrapper.find(DetailBanner).exists()).toBe(true)
  })
})