import axios from "axios"

const baseUrl = "https://ghibliapi.herokuapp.com/"

let warhead

async function init() {
  try {
    const result = await warhead
    // console.log(result.data)
    return result.data
  } catch (error) {
    console.log(error.message)
    return error
  }
}
export function Get(url) {
  warhead = axios.get(`${baseUrl}${url}`)
  return init()
}
export function Post(url, payload) {
  warhead = axios.post(url, payload)
  return init()
}
